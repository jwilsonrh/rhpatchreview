#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# rhpatchreview - Red Hat Patch Reviewing Tool
#
# This tool is intended for RHEL kernel patch review, complete with
# hooks into git and gitlab.
#
# Copyright (C) 2012-2021 Red Hat Inc.
# Author: Jarod Wilson <jarod@redhat.com>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.  See http://www.gnu.org/copyleft/gpl.html for
# the full text of the license.

import os
import sys
import re
import optparse
import tempfile
from subprocess import Popen, PIPE
# python3-dialog
import dialog
# python3-GitPython
import git
# python3-gitdb
from gitdb.exc import BadName, BadObject
# python3-gitlab
import gitlab
from pathlib import Path
from urllib import parse

# our core library, shared with rhcheckpatch
from rhpatchparser import Match as m
import rhpatchparser as pp

COMMENT_HEADER = ("# This file is submitted as a GitLab comment when closed. You can\n"
                  "# abort submitting a comment by making this file empty. Lines that\n"
                  "# start with a hash (#) will not be sent.\n")


def get_dialog_height_width():
    p = Popen(["tput", "lines"], stdout=PIPE)
    h = int(p.communicate()[0]) - 8
    p = Popen(["tput", "cols"], stdout=PIPE)
    w = int(p.communicate()[0]) - 8
    return (h, w)


class RHCommit:

    def __init__(self, commit):
        self.commit = commit
        self.match = m.NOUCID
        self.ucommit = None
        self.info = ""
        self.header = ""
        self.feedback = ""


class RHMergeRequest:

    def __init__(self, opts, gl_instance, gl_project, gl_mergerequest):
        self.inst = gl_instance
        self.proj = gl_project
        self.mreq = gl_mergerequest
        self.id = gl_mergerequest.iid
        self.submitter = ""
        self.summary = ""
        self.header = ""
        self.feedback = ""
        self.pipeline_info = "Pipeline: "
        self.full_match = True
        self.has_deps = False
        self.commits = []
        self.opts = opts
        self.pcount = 0
        self.diffs = (None, None)
        self.repo = None
        self.remotes = []


def show_mr_vimdiff(prior, current, revs):
    prior_rev = int(revs) - 1
    prior_f = tempfile.NamedTemporaryFile(mode="w")
    current_f = tempfile.NamedTemporaryFile(mode="w")
    prior_f.write("Full Merge Request diff, v%d\n" % prior_rev)
    prior_f.write("%s\n" % prior)
    prior_f.flush()
    current_f.write("Full Merge Request diff, v%d\n" % revs)
    current_f.write("%s\n" % current)
    current_f.flush()
    os.system("vimdiff %s %s" % (prior_f.name, current_f.name))
    prior_f.close()
    current_f.close()


def show_vimdiff(opts, commit_id, submission):
    fu = tempfile.NamedTemporaryFile(mode="w")
    fs = tempfile.NamedTemporaryFile(mode="w")
    g = git.Git(opts.gitrepo)
    print(commit_id)
    upstream_diff = g.diff("%s^" % commit_id, commit_id)

    fu.write("Upstream commit %s\n" % commit_id)
    fu.write("%s\n" % upstream_diff)
    fu.flush()
    fs.write("RHEL patch submission\n")
    fs.write("%s" % submission)
    fs.flush()
    os.system("vimdiff %s %s" % (fu.name, fs.name))
    fu.close()
    fs.close()


def show_diff(mr_desc, message, diff):
    diffstat = pp.diffstat(diff)
    f = tempfile.NamedTemporaryFile(suffix=".patch", mode="w")
    f.write("%s\n" % '\n'.join(mr_desc.split('\n')[:30]))
    if len(mr_desc.split('\n')) > 30:
        f.write("(truncated at 30 lines)\n")
    f.write("---[End MR Desc]---\n")
    f.write("%s\n" % message)
    f.write("%s\n--- \n" % diffstat)
    f.write("%s\n" % diff)
    f.flush()
    os.system("vim %s" % f.name)
    f.close()


def get_pipeline_info(rhmr):
    if rhmr.mreq.head_pipeline is None:
        return None

    pipeline_id = rhmr.mreq.head_pipeline.get('id')
    pipeline = rhmr.proj.pipelines.get(pipeline_id)

    if pipeline is None:
        rhmr.pipeline_info += "no pipeline found\n"
        return None

    for label in rhmr.mreq.labels:
        if label.startswith("CKI::"):
            ext_status = ""
            label_parts = label.split("::")
            base_status = label_parts[1]
            if len(label_parts) > 2:
                ext_status = " on " + label_parts[2] + " stage"
            status = f"{base_status}{ext_status}"

    rhmr.pipeline_info += f"{status} ({pipeline.web_url})\n"

    return status


def show_info_and_actions(rhmr, rhcommit, d):
    d.add_persistent_args(["--backtitle", "Patch Review Console"])
    d.add_persistent_args(["--title", "Patch Metadata for MR%s" % rhmr.id])
    d.add_persistent_args(["--ok-label", "Select Task"])
    d.add_persistent_args(["--cancel-label", "Exit"])
    choices = []
    commit = rhcommit.commit
    diff = commit.diff()

    choices.append(["Done", "Done with this patch, go to next, if any"])

    if rhcommit.match not in (m.NOUCID, m.RHELONLY, m.POSTED, m.MERGECOMMIT):
        choices.append(["Vimdiff", "See vimdiff(s) between upstream commit(s) and submission"])

    if rhcommit.match == m.KABI:
        choices.append(["Peek-KABI-Awooga", "Peek at the patch and scrutinize kABI issues"])
    else:
        choices.append(["Peek", "Peek at the patch in vim"])

    if rhcommit.match in (m.NOUCID, m.POSTED):
        choices.append(["Override", "See vimdiff between override hash and submission"])

    if rhmr.diffs[0] is not None and rhmr.diffs[1] is not None:
        choices.append(["Set-vimdiff", "See vimdiff with prior set version"])

    choices.append(["Note", "Add review note for this patch"])

    mh = len(choices)
    (h, w) = get_dialog_height_width()

    diffstr = pp.get_submitted_diff(diff)
    while 1:
        (code, val) = d.menu(text=rhcommit.info, height=h, width=w, menu_height=mh, choices=choices)
        if code == d.DIALOG_OK:
            if val == "Done":
                return True
            elif val == "Vimdiff":
                show_vimdiff(rhmr.opts, rhcommit.ucommit, diffstr[0])
            elif val == "Override":
                (icode, override) = d.inputbox("Hash to compare with patch \'%s\'" % commit.title, init="", height=h, width=w)
                if icode == d.DIALOG_OK and override:
                    show_vimdiff(rhmr.opts, override, diffstr[0])
            elif val in ("Peek-KABI-Awooga", "Peek"):
                show_diff(rhmr.mreq.description, commit.message, diffstr[0])
            elif val == "Set-vimdiff":
                revs = len(rhmr.mreq.diffs.list())
                show_mr_vimdiff(rhmr.diffs[0], rhmr.diffs[1], revs)
            elif val == "Note":
                rhcommit.feedback += "  "
                (icode, note) = d.inputbox("Review note for patch \'%s\'" % commit.title, init="", height=h, width=w)
                rhcommit.feedback += note + "  \n"
            else:
                return False
        else:
            return False

    return True


def format_commit_string(commit, pnum, ptotal):
    commit_info = ""
    commit_info = "[%s of %s] %s (\"%s\")  " % (pnum, ptotal, commit.short_id, commit.title)
    return commit_info


def find_git_commit_source_branch(rhmr, ucommit):
    # Start with local branches (linus' master, net, net-next)
    branches = rhmr.repo.git.branch("--contains", ucommit)
    blist = []
    for branch in branches.splitlines():
        blist.append(branch.replace("*", "").replace("+", "").strip())
    if "master" in blist:
        return "Linus"
    # Favor non-tip and non-linux-next
    if len(blist) > 1 and "tip" in blist:
        blist.remove("tip")
    if len(blist) > 1 and "linux-next" in blist:
        blist.remove("linux-next")
    for branch in blist:
        if branch in rhmr.remotes:
            return branch

    # Now look at remote branches if we still have nothing
    branches = rhmr.repo.git.branch("-a", "--contains", ucommit)
    for branch in branches.splitlines():
        blist.append(branch.strip())
    # Favor non-tip and non-linux-next
    if len(blist) > 1 and "tip" in blist:
        blist.remove("tip")
    if len(blist) > 1 and "linux-next" in blist:
        blist.remove("linux-next")
    for branch in blist:
        if branch.startswith("remotes/"):
            remote = branch.split("/")[1]
            if remote in rhmr.remotes:
                return remote

    return "Unknown"


def compare_with_detected_gitref(rhmr, gitrefs, patch_index):
    rhcommit = rhmr.commits[patch_index]
    commit = rhcommit.commit
    commit_found = False
    pnum = patch_index + 1
    commit_info = format_commit_string(commit, pnum, rhmr.pcount)
    gitrepo = rhmr.opts.gitrepo

    csi = commit.short_id
    for gitref in gitrefs:
        if gitref == "RHELonly":
            rhcommit.info += f"Upstream: {csi} is a RHEL-only patch\n"
            rhcommit.header += f"- RHEL-only: {commit_info}\n"
            rhcommit.match = m.RHELONLY
            rhcommit.ucommit = gitref
            return
        elif gitref == "Posted":
            rhcommit.info += f"Upstream: {csi} is Posted\n"
            rhcommit.header += f"- Posted: {commit_info}\n"
            rhcommit.match = m.POSTED
            rhcommit.ucommit = gitref
            return
        elif gitref == "Merge":
            rhcommit.info += f"Upstream: {csi} is a merge commit\n"
            rhcommit.header += f"- Merge: {commit_info}\n"
            rhcommit.match = m.MERGECOMMIT
            rhcommit.ucommit = gitref
            return

    for gitref in gitrefs:
        try:
            ucommit = rhmr.repo.commit(gitref)
            commit_found = True
            break
        except (BadObject, BadName, ValueError):
            rhcommit.info += f"NoUCID: commit {gitref} not found in {gitrepo}\n"
            rhcommit.header += f"- NoUCID: {commit_info}\n"
            rhcommit.match = m.NOUCID
            rhcommit.ucommit = None

    if not commit_found:
        return

    ubranch = find_git_commit_source_branch(rhmr, ucommit)
    udiff = rhmr.repo.git.diff("%s^" % ucommit, ucommit)
    diff = pp.get_submitted_diff(commit.diff())
    interdiff = pp.compare_commits(udiff, diff[0])
    udata = f'[{ubranch}:{gitref[:8]}]  '
    if len(interdiff) == 0:
        rhcommit.info += f"Full: Submission {csi} matches upstream {ubranch} commit {gitref} perfectly\n"
        if rhmr.opts.autoskip:
            sys.stdout.write("* %s\n" % commit_info)
            sys.stdout.write("  matches upstream %s commit %s perfectly\n" % (ubranch, gitref))
        rhcommit.header += f"- {commit_info}{udata}\n"
        rhcommit.match = m.FULL
        rhcommit.ucommit = gitref
        return

    mydiff = pp.get_submitted_diff(commit.diff())
    udiff_part = pp.get_partial_diff(udiff, mydiff[1])
    idiff_part = pp.compare_commits(udiff_part, mydiff[0])
    if len(idiff_part) == 0:
        rhcommit.info += f"Partial: Submission {csi} matches partial upstream {ubranch} commit {gitref} perfectly\n"
        if rhmr.opts.autoskip:
            sys.stdout.write("* Partial: %s\n" % commit_info)
            sys.stdout.write("  matches partial upstream %s commit %s perfectly\n" % (ubranch, gitref))
        rhcommit.header += f"- Partial: {commit_info}{udata}\n"
        rhcommit.match = m.PARTIAL
        rhcommit.ucommit = gitref
        return

    if rhmr.opts.filterdiff_excludes:
        excludes = rhmr.opts.filterdiff_excludes
        interdiff = pp.compare_commit_to_upstream_filtered(udiff, diff[0], excludes)
        if len(interdiff) == 0:
            rhcommit.info += f"Partial: Submission {csi} matches upstream {ubranch} commit {gitref} after filtering\n"
            if rhmr.opts.autoskip:
                sys.stdout.write("* Partial: %s\n" % commit_info)
                sys.stdout.write("  matches upstream %s commit %s perfectly after filtering\n" % (ubranch, gitref))
            rhcommit.header += f"- Filtered: {commit_info}{udata}\n"
            rhcommit.match = m.PARTIAL
            rhcommit.ucommit = gitref
            return

    if pp.raise_kabi_red_flag(commit.message, diff[0]):
        rhcommit.info += f"KABI: Submission {csi} not identical to upstream {ubranch} commit {gitref}\n"
        rhcommit.header += f"- KABI: {commit_info}{udata}\n"
        rhcommit.match = m.KABI
        rhcommit.ucommit = gitref
        return

    rhcommit.info += f"Diffs: Submission {csi} not identical to upstream {ubranch} commit {gitref}\n"
    rhcommit.header += f"- Diffs: {commit_info}{udata}\n"
    rhcommit.match = m.DIFFS
    rhcommit.ucommit = gitref

    return


def review_commit(rhmr, d, patch_index):
    perfecto = False
    compare = True
    match = m.NOUCID
    gitref = None
    bad_format_gitref = False

    rhcommit = rhmr.commits[patch_index]
    commit = rhcommit.commit
    message = commit.message
    ptotal = rhmr.pcount
    mreq = rhmr.mreq
    pnum = patch_index + 1

    if not rhmr.opts.quiet:
        approvals = mreq.approvals.get()
        rhcommit.info += f"MR#{mreq.iid}: [{rhmr.proj.name}/{mreq.target_branch} ({pnum}/{ptotal})] {commit.title} ({mreq.state})\n"
        rhcommit.info += f"Author: {commit.author_name} <{commit.author_email}>\n"
        rhcommit.info += rhmr.pipeline_info
        rhcommit.info += f"Labels:"
        for label in mreq.labels:
            subsystem_acks = len(label.split("::")) == 3
            if not label.startswith("Subsystem:") and not subsystem_acks:
                rhcommit.info += f" {label},"
        rhcommit.info += f"\n"
        rhcommit.info += f"Approved-by:"
        for approval in approvals.approved_by:
            rhcommit.info += f" {approval['user']['name']},"
        rhcommit.info += f"\n"

    ref = rhmr.proj.commits.get(commit.id)
    gitrefs = pp.extract_ucid(message, ref.parent_ids)
    if len(gitrefs) == 0:
        gitrefs = pp.extract_ucid_greedy(message)
        bad_format_gitref = True

    if len(gitrefs) > 1:
        rhcommit.feedback += "  WARNING: multiple git references found in this submission!  \n"
    elif len(gitrefs) == 0:
        rhcommit.header   += "- NoUCID: %s\n" % format_commit_string(commit, pnum, ptotal)
        rhcommit.feedback += "  no upstream references found in this submission!  \n"
        compare = False

    if rhmr.opts.gitrepo:
        if compare:
            compare_with_detected_gitref(rhmr, gitrefs, patch_index)
            if bad_format_gitref == True:
                rhcommit.feedback += "  gitref line format incorrect, will fail webhook CommitRefs check  \n"
    else:
        rhcommit.header += "N/A: %s\n" % commit.title
        for gitref in gitrefs:
            if gitref == "RHELonly":
                rhcommit.info += "Upstream: RHEL-only patch\n"
                rhcommit.match = m.RHELONLY
                break
            elif gitref is not None:
                rhcommit.info += f"Upstream: {gitref.strip()}\n"

    if not re.search("redhat.com", commit.author_email):
        rhcommit.feedback += "  patch not from a Red Hat email address (use git commit --amend --reset-author to remedy)  \n"

    if rhmr.submitter[1] != commit.author_email:
        rhcommit.feedback += f"  authorship mismatch: {commit.author_email} != {rhmr.submitter[1]}\n"

    found_sig = False
    for line in message.split("\n"):
        if line.startswith("Signed-off-by:"):
            if "redhat.com" in line:
                found_sig = True

    if not found_sig:
        rhcommit.feedback += "  no valid redhat.com DCO/Signed-off-by: found  \n"

    perfecto = (rhcommit.match == m.FULL or rhcommit.match == m.PARTIAL)
    if perfecto and rhmr.opts.autoskip:
        return rhcommit.match

    bzurlpfx="https://bugzilla.redhat.com/show_bug.cgi?id="
    bugs = pp.extract_bzs(message)

    if len(bugs) > 0:
        for bug in bugs:
            rhcommit.info += "Bugzilla: "
            if not rhmr.opts.quiet:
                rhcommit.info += f"{bzurlpfx}"
            rhcommit.info += f"{bug}\n"
    elif rhcommit.match != m.MERGECOMMIT:
        rhcommit.feedback += "  no bugzilla link found in submission  \n"

    cves = pp.find_cve_numbers(commit.title, message)
    for cve in cves:
        rhcommit.info += f"CVE ref: {bzurlpfx}{cve}\n"

    return show_info_and_actions(rhmr, rhcommit, d)


def review_notes(notes):
    n = tempfile.NamedTemporaryFile(suffix=".txt", mode="r+")
    n.write("%s\n" % notes)
    n.flush()
    os.system("vim %s" % n.name)

    return n


def show_mr_information(d, header):
    d.add_persistent_args(["--backtitle", "Merge Request Information"])
    (h, w) = get_dialog_height_width()
    d.scrollbox(text=header, height=h, width=w)


def leave_mr_feedback(rhmr, d, todo):
    d.add_persistent_args(["--backtitle", "Leave Merge Request Feedback"])
    d.add_persistent_args(["--title", "Details for MR%s" % rhmr.id])
    d.add_persistent_args(["--ok-label", "Choose"])
    d.add_persistent_args(["--cancel-label", "Quit"])
    choices = []

    # my_ack = f"Acked-by: {rhmr.inst.user.name} <{rhmr.inst.user.public_email}>"
    my_nack = f"Nacked-by: {rhmr.inst.user.name} <{rhmr.inst.user.public_email}>"
    mr_is_mine = (rhmr.inst.user.name == rhmr.mreq.author['name'])

    if mr_is_mine:
        rhmr.header += "\nThis looks like a merge request you submitted.\n"
    else:
        rhmr.header += f"\nYou can provide feedback on {rhmr.proj.name} MR{rhmr.id}.\n\n"

    already_acked = False
    approvals = rhmr.mreq.approvals.get()
    for approval in approvals.approved_by:
        if rhmr.inst.user.name == approval['user']['name']:
            rhmr.header += "You have Approved this Merge Request.\n"
            already_acked = True

    choices.append(["Quit", "Quit out of Merge Request review console"])

    if not mr_is_mine:
        if not already_acked:
            choices.append(["Approve", "Approve this Merge Request"])
        else:
            choices.append(["Unapprove", "Unapprove this Merge Request"])

    choices.append(["Nack", "Nack this Merge Request"])
    choices.append(["Comment", "Comment on this Merge Request (leaves comment on vim exit)"])
    if rhmr.opts.todo and todo is not None:
        choices.append(["TODO-Done", "Mark this MR's TODO item as Done"])
    choices.append(["View", "View existing comments on this Merge Request"])

    if rhmr.diffs[1] is not None:
        choices.append(["Peek", "Peek at the full MR patch in vim"])

    if rhmr.diffs[0] is not None and rhmr.diffs[1] is not None:
        choices.append(["Set-vimdiff", "See vimdiff with prior set version"])

    mh = len(choices)
    (h, w) = get_dialog_height_width()

    mark_done = False
    while 1:
        (code, val) = d.menu(text=rhmr.header, height=h, width=w, menu_height=mh, choices=choices)
        if code == d.DIALOG_OK:
            if val == "Quit":
                return mark_done
            elif val == "Approve":
                approval = rhmr.mreq.approve(sha=rhmr.mreq.diff_refs['head_sha'])
                rhmr.header += "You have Approved this Merge Request.\n"
                if rhmr.opts.todo and todo is not None:
                    todo.mark_as_done()
                mark_done = True
            elif val == "Unapprove":
                approval = rhmr.mreq.unapprove()
                rhmr.header += "You have Unapproved this Merge Request.\n"
            elif val == "Nack":
                if already_acked:
                    approval = rhmr.mreq.unapprove()
                discussion = rhmr.mreq.discussions.create({'body': f'{my_nack}'})
            elif val == "Comment":
                comments = COMMENT_HEADER + rhmr.feedback
                comment_file = review_notes(comments)
                comment_file.flush()
                comment_file.seek(0)
                comment = comment_file.read()
                comment = Path(comment_file.name).read_text()
                filtered_comment = ""
                for cline in comment.splitlines():
                    if not cline.startswith("#"):
                        filtered_comment += cline + '  \n'
                if filtered_comment != "":
                    discussion = rhmr.mreq.discussions.create({'body': f'{filtered_comment}'})
                comment_file.close()
            elif val == "Set-vimdiff":
                revs = len(rhmr.mreq.diffs.list())
                show_mr_vimdiff(rhmr.diffs[0], rhmr.diffs[1], revs)
            elif val == "TODO-Done":
                if rhmr.opts.todo and todo is not None:
                    todo.mark_as_done()
                mark_done = True
            elif val == "View":
                notes = ""
                discussions = rhmr.mreq.discussions.list()
                for disc in discussions:
                    discussion = rhmr.mreq.discussions.get(disc.id)
                    for note in discussion.attributes['notes']:
                        notes += f"\n----[From: {note['author']['name']}]----\n"
                        notes += note['body']
                        notes += "\n"
                note_file = review_notes(notes)
                note_file.close()
            elif val == "Peek":
                show_diff(rhmr.mreq.description, "", rhmr.diffs[1])
            else:
                return mark_done
        else:
            return mark_done


def read_options_from_gitconfig(opts):
    gitconfig = git.GitConfigParser([os.path.normpath(os.path.expanduser("~/.gitconfig"))], read_only=True)
    if not opts.gitrepo:
        try:
            opts.gitrepo = gitconfig.get_value('rhpatchreview', 'gitrepo')
        except:
            opts.gitrepo = ""
    if not opts.autoskip:
        try:
            opts.autoskip = gitconfig.get_value('rhpatchreview', 'autoskip')
        except:
            opts.autoskip = False
    if not opts.no_feedback:
        try:
            opts.no_feedback = gitconfig.get_value('rhpatchreview', 'nofeedback')
        except:
            opts.no_feedback = False
    if not opts.filterdiff_excludes:
        try:
            opts.filterdiff_excludes = gitconfig.get_value('rhpatchreview', 'filterdiffexcludes')
        except:
            opts.filterdiff_excludes = ""

    return opts


def process_merge_request(opts, gl, d, todo):
    url_parts = parse.urlsplit(opts.mr)
    instance_url = parse.urlunparse(url_parts[:2] + ('',) * 4)
    if not instance_url.startswith(gl.url):
        sys.stdout.write("GitLab server %s doesn't match config: %s\n" % (opts.mr, gl.url))
        exit(1)
    match = re.match(r'/(.*)/merge_requests/(\d+)$', url_parts.path)
    if not match:
        sys.stdout.write("MR URL %s doesn't match r'/(.*)/merge_requests/(\d+)$'\n" % opts.mr)
        exit(1)
    project_path = re.sub('/-$', '', match[1])
    gl_proj = gl.projects.get(project_path)
    mr_id = int(match[2])
    mreq = gl_proj.mergerequests.get(mr_id)
    approvals = mreq.approvals.get()
    merge_commits = 0

    rhmr = RHMergeRequest(opts, gl, gl_proj, mreq)

    submitter_email = gl.users.get(mreq.author['id']).public_email
    rhmr.submitter = (mreq.author['name'], submitter_email)
    rhmr.summary += f"Submitter: {mreq.author['name']} <{submitter_email}>\n"
    rhmr.summary += f"MR-URL/State: {opts.mr} ({mreq.state})\n"
    rhmr.summary += f'Title: [{gl_proj.name}/{mreq.target_branch}] {mreq.title}\n'

    pipeline_status = get_pipeline_info(rhmr)
    rhmr.summary += rhmr.pipeline_info

    rhmr.header = rhmr.summary + '\n'
    rhmr.summary += f'{mreq.labels}\n'
    if len(approvals.approved_by) > 0:
        approvers = "Approved-by: "
        for approval in approvals.approved_by:
            approvers += f"{approval['user']['name']}, "
        approvers = approvers.strip(", ")
        rhmr.header += f'{approvers}\n'
        rhmr.summary += f'{approvers}\n'
    rhmr.summary += f'\nDescription:\n------------\n{mreq.description}\n------------\n\n'

    if opts.gitrepo:
        rhmr.repo = git.Repo(opts.gitrepo)
        remote_list = rhmr.repo.git.remote("-v", "show").splitlines()
        for remote in remote_list:
            if remote.endswith("(fetch)"):
                remote = remote.split("\t")[0].strip()
                rhmr.remotes.append(remote)

    series_start = mreq.diff_refs['start_sha']
    for label in mreq.labels:
        if label.startswith("Dependencies::"):
            dep_scope = label.split("::")[-1]
            if dep_scope != "OK":
                rhmr.has_deps = True
                series_start = dep_scope
                sys.stdout.write("Series start from MR label: %s\n" % series_start)

    diff_ids = pp.mr_get_diff_ids(mreq)
    diff_diffs = None
    number_of_revs = len(diff_ids)
    if number_of_revs >= 2:
        rhmr.summary += f'Patchset has {number_of_revs} revisions, '
        rhmr.header += f'* Patchset has {number_of_revs} revisions, '
        (latest, old) = pp.mr_get_last_two_diff_ranges(mreq, diff_ids)
        (old_diff, new_diff) = pp.get_diffs_from_mr(mreq, diff_ids, latest, old)
        if old_diff is not None and new_diff is not None:
            diff_diffs = pp.compare_commits(old_diff, new_diff)
            rhmr.diffs = (old_diff, new_diff)

        if diff_diffs is not None and len(diff_diffs) > 0:
            rhmr.summary += f'and there are '
            rhmr.header += f'and there are '
        else:
            rhmr.summary += f'but there are no '
            rhmr.header += f'but there are no '
        rhmr.summary += f'code differences between this version and the prior one.\n\n'
        rhmr.header += f'code differences between this version and the prior one.\n'

    last_diff = None
    for diff_id in diff_ids:
        last_diff = diff_id

    diff_data = mreq.diffs.get(last_diff)
    full_diff = pp.get_submitted_diff(diff_data.diffs)
    if rhmr.diffs == (None, None):
        rhmr.diffs = (None, full_diff[0])
    dep_note = ""
    if rhmr.has_deps:
        dep_note = " (includes dependencies)"

    desc_has_diffstat = False
    diffstat_re = re.compile(r" \d* files? changed, (\d* insertions?\(\+\))?(, )?(\d* deletions?\(\-\))?")
    for line in mreq.description.splitlines():
        match = diffstat_re.match(line)
        if match:
            desc_has_diffstat = True
            break

    if not desc_has_diffstat:
        diffstat = pp.diffstat(full_diff[0])
        rhmr.summary += f'MR diffstat{dep_note}:\n'
        rhmr.summary += f'{diffstat}\n'

    commits = mreq.commits()
    i = 0
    dep_count = 0
    rhmr.pcount = len(commits)
    commit_list = []
    for commit in commits:
        # Skip merge commits, consider first one start of deps if label missing
        if commit.title.startswith("Merge remote-tracking branch"):
            ref = gl_proj.commits.get(commit.id)
            if len(ref.parent_ids) > 1:
                sys.stdout.write("Merge commit: %s\n" % commit.id)
                merge_commits += 1
                if not rhmr.has_deps:
                    sys.stdout.write("Series start from merge commit: %s\n" % commit.id)
                    series_start = commit.short_id
                    rhmr.has_deps = True
        if rhmr.has_deps and commit.id.startswith(series_start):
            dep_count = rhmr.pcount - i
            rhmr.pcount = i
            break
        commit_list.append(RHCommit(commit))
        i += 1
    dep_count -= merge_commits
    rhmr.summary += f'Number of commits in MR: {rhmr.pcount}\n\n'
    rhmr.header += f'* Number of commits in MR: {rhmr.pcount}\n'
    for rhcommit in reversed(commit_list):
        rhmr.commits.append(rhcommit)
    if rhmr.has_deps:
        rhmr.summary += f'* Number of dep commits in MR: {dep_count}\n'
        rhmr.header += f'* Number of dep commits in MR: {dep_count}\n'

    if opts.project or opts.todo:
        show_mr_information(d, rhmr.summary)

        approvals = mreq.approvals.get()
        for approval in approvals.approved_by:
            if rhmr.inst.user.name == approval['user']['name']:
                sys.stdout.write("You have already Approved this Merge Request, skipping review.\n")
                mark_done = leave_mr_feedback(rhmr, d, todo)
                return mark_done

    if opts.info:
        show_mr_information(d, rhmr.summary)
        exit(0)

    if opts.gitrepo:
        rhmr.summary += f'Kernel git remotes used for UCID comparison: {rhmr.remotes}\n\n'

    rhmr.summary += f'Per-patch evaluations:\n'
    rhmr.summary += f'----------------------\n'
    rhmr.summary += f'- [0 of {rhmr.pcount}] {gl_proj.name}/{mreq.target_branch} ("{mreq.title}")  \n'
    if pipeline_status is None:
        rhmr.summary += "  no pipeline data found for this MR  \n"
    elif pipeline_status == "Running" or pipeline_status == "Pending":
        rhmr.summary += f"  Note: pipeline is still {pipeline_status}  \n"
    elif pipeline_status != "Success" and pipeline_status != "OK":
        rhmr.summary += f"  pipeline found for this MR was not successful ({pipeline_status})  \n"

    bugs = pp.extract_bzs(mreq.description)

    if len(bugs) == 0:
        rhmr.feedback += f'- [0 of {rhmr.pcount}] {gl_proj.name}/{mreq.target_branch} ("{mreq.title}")  \n'
        rhmr.feedback += "  no bugzilla link found in MR description  \n"
        rhmr.summary += "  no bugzilla link found in MR description  \n"

    patch_index = opts.start - 1
    all_match = True
    for rhcommit in rhmr.commits:
        keep_going = review_commit(rhmr, d, patch_index)
        patch_index += 1
        if patch_index == rhmr.pcount:
                break
        if rhcommit.match != m.FULL and rhcommit.match != m.PARTIAL:
            all_match = False
        if not keep_going:
            return False

    for rhcommit in rhmr.commits:
        rhmr.summary += rhcommit.header
        if len(rhcommit.feedback) > 0:
            rhmr.summary += rhcommit.feedback
            rhmr.feedback += rhcommit.header
            rhmr.feedback += rhcommit.feedback

    # view compiled patch review notes for set
    note_file = review_notes(rhmr.summary)
    note_file.close()

    # leave MR review feedback, if this functionality is enabled
    if all_match:
        rhmr.header += '* All commits in MR match upstream\n'
    else:
        rhmr.header += '* This set contains variances from upstream\n'
    mark_done = False
    if not opts.no_feedback:
        mark_done = leave_mr_feedback(rhmr, d, todo)

    _ = os.system("clear")

    return mark_done


def get_mr_from_todo_list(opts, gl, d):
    d.add_persistent_args(["--backtitle", "Merge Request Listing for %s" % opts.project])
    d.add_persistent_args(["--title", "Pick an MR..."])
    d.add_persistent_args(["--ok-label", "Choose"])
    d.add_persistent_args(["--cancel-label", "Quit"])
    choices = []

    choices.append(["Quit", "Quit out of Merge Request review console"])

    header = f"List of pending Merge Requests on your TODO list\n"

    todo_list = gl.todos.list(state='pending', type='MergeRequest')
    if len(todo_list) == 0:
        sys.stdout.write("Your GitLab todo list has no Merge Requests in it, congrats!\n")
        exit(0)

    for todo in todo_list:
        mr_id = todo.target['iid']
        path = todo.project['path']
        title = todo.target['title']
        author = todo.target['author']['name']
        if gl.user.name != author:
            choices.append([f'{path}/{mr_id}', f"{title} // from {author}"])

    mh = len(choices)
    (h, w) = get_dialog_height_width()

    while 1:
        (code, val) = d.menu(text=header, height=h, width=w, menu_height=mh, choices=choices)
        if code == d.DIALOG_OK:
            if val == "Quit":
                return val
            else:
                todo = None
                path = val.split("/")[0]
                mr_id = int(val.split("/")[1])
                for todo in todo_list:
                    if path == todo.project['path'] and mr_id == int(todo.target['iid']):
                        break
                proj_path = todo.project['path_with_namespace']
                title = todo.target['title']
                author = todo.target['author']['name']
                opts.mr = f'{gl.url}/{proj_path}/-/merge_requests/{mr_id}'
                mark_done = process_merge_request(opts, gl, d, todo)
                if mark_done:
                    choices.remove([f"{path}/{mr_id}", f"{title} // from {author}"])
        else:
            return "Quit"

    return None


def get_mr_from_project_index(opts, gl, d, project_id):
    gl_proj = gl.projects.get(project_id)
    mr_list = gl_proj.mergerequests.list(state="opened")

    if len(mr_list) == 0:
        sys.stdout.write("No Merge Requests in %s you need to deal with, congrats!\n" % gl_proj.name)
        exit(0)

    d.add_persistent_args(["--backtitle", "Merge Request Listing for %s" % opts.project])
    d.add_persistent_args(["--title", "Pick an MR..."])
    d.add_persistent_args(["--ok-label", "Choose"])
    d.add_persistent_args(["--cancel-label", "Quit"])
    choices = []

    choices.append(["Quit", "Quit out of Merge Request review console"])

    header = f"List of Merge Requests for {opts.project}\n"

    for mreq in mr_list:
        already_acked = False
        approvals = mreq.approvals.get()
        for approval in approvals.approved_by:
            if gl.user.name == approval['user']['name']:
                already_acked = True
        if not already_acked:
            choices.append([f'{mreq.iid}', f"{mreq.title} // from {mreq.author['name']}"])

    mh = len(choices)
    (h, w) = get_dialog_height_width()

    while 1:
        (code, val) = d.menu(text=header, height=h, width=w, menu_height=mh, choices=choices)
        if code == d.DIALOG_OK:
            if val == "Quit":
                return val
            else:
                opts.mr = f"{gl.url}/{gl_proj.path_with_namespace}/-/merge_requests/{val}"
                my_approval = process_merge_request(opts, gl, d, None)
                if my_approval:
                    for mreq in mr_list:
                        if int(mreq.iid) == int(val):
                            choices.remove([f'{mreq.iid}',
                                            f"{mreq.title} // from {mreq.author['name']}"])
                            break
        else:
            return "Quit"

    return None


def main(argv):
    parser = optparse.OptionParser(usage='%prog [-g <gitrepo>] [-a] [-i] [-f <file>] [-g <gitrepo>] [-q] [-n] [-s <patch #>] ([-m] <gitlab MR url> | [-p] <project> | -t)', version="%prog 0.2")
    parser.add_option('-a', '--autoskip', action='store_true', dest='autoskip', help='automatically skip over showing info for patches that match upstream commits perfectly')
    parser.add_option('-f', '--filter', action='store', dest='filterdiff_excludes', help='filterdiff exclude file, listing files to exclude from upstream diffs')
    parser.add_option('-g', '--gitrepo', action='store', dest='gitrepo', help='check discovered git hashes against specified upstream git tree')
    parser.add_option('-i', '--info', action='store_true', dest='info', help='Show MR info')
    parser.add_option('-m', '--merge-request', action='store', dest='mr', help='Merge Request URL')
    parser.add_option('-n', '--no-feedback', action='store_true', dest='no_feedback', help='disable UI to leave feedback on the MR')
    parser.add_option('-p', '--project', action='store', dest='project', help='Show all MRs for project')
    parser.add_option('-s', '--start', action='store', type="int", dest='start', help='Start review at patch #')
    parser.add_option('-t', '--todo', action='store_true', dest='todo', help='Show all MRs from your GitLab TODO list')
    parser.add_option('-q', '--quiet', action='store_true', dest='quiet', help='be quiet')

    (opts, args) = parser.parse_args()

    projects = {'rhel-9': 24163133, 'rhel-8': 24118165, 'rhel-7': 24140364,
                'rhel-alt-7': 24326826, 'rhel-6': 24155748, 'kernel-test': 20363472,
                'kernel-ark': 13604247}

    opts = read_options_from_gitconfig(opts)

    try:
        gl = gitlab.Gitlab.from_config()
        gl.auth()
    except Exception:
        parser.error("Please set up ~/.python-gitlab.cfg!\n" \
                     " => See https://python-gitlab.readthedocs.io/en/stable/cli.html")

    if opts.project and opts.todo:
        parser.error("Cannot run in both project and todo list mode simultaneously\n")

    if not opts.start:
        opts.start = 1

    d = dialog.Dialog(dialog="dialog")
    if opts.project:
        if opts.project not in projects.keys():
            parser.error("Unknown project (%s) specified. Known projects are %s.\n"
                         % (opts.project, projects))
        while 1:
            opts.mr = get_mr_from_project_index(opts, gl, d, projects[opts.project])
            if opts.mr == "Quit":
                _ = os.system("clear")
                return 0

    if opts.todo:
        while 1:
            opts.mr = get_mr_from_todo_list(opts, gl, d)
            if opts.mr == "Quit":
                _ = os.system("clear")
                return 0

    # if any args are left, assume it's a gitlab MR url w/o -m specified
    if not opts.mr and args:
        if len(args) > 1:
            parser.error("Too many args. We need a single gitlab MR url.")

        opts.mr = args[0]

    if not opts.mr:
        parser.error("No gitlab MR url specified.")

    process_merge_request(opts, gl, d, None)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
